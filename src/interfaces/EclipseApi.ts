/** ***************************************************************
 Copyright (C) 2022 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
//
// ECLIPSE PROJECT
//
export interface Repo {
  url: string;
  repo?: string;
  org?: string;
}

export interface UserRef {
  username: string;
  full_name?: string;
  url: string;
}

export interface Release {
  name: string;
  url: string;
}
export interface WorkingGroup {
  name: string;
  id: string;
}

export interface GitlabRepoSettings {
  project_group: string;
  ignored_sub_groups: string[];
}

export interface EclipseProject {
  project_id: string;
  short_project_id: string;
  name: string;
  summary: string;
  url: string;
  website_url: string;
  website_repo: string[];
  logo: string;
  tags: string[];
  gitlab: GitlabRepoSettings;
  github_repos: Repo[];
  gerrit_repos: Repo[];
  contributors: UserRef[];
  committers: UserRef[];
  project_leads: UserRef[];
  working_groups: WorkingGroup[];
  spec_project_working_group: WorkingGroup | Array<null>;
  state: string;
  top_level_project: string;
  releases: Release[];
}

//
// ECLIPSE INTEREST GROUP
//

export interface Description {
    summary: string;
    full: string;
}

export interface Scope {
    summary: string;
    full: string;
}

export interface InterestGroup {
    id: string;
    title: string;
    description: Description;
    logo: string;
    scope: Scope;
    leads: UserRef[];
    participants: UserRef[];
    foundationdb_project_id: string;
    state: string;
    gitlab: GitlabRepoSettings;
}

//
// ECLIPSE ACCOUNT
//

export interface EclipseUser {
  uid: string;
  name: string;
  mail: string;
  picture: string;
  eca: {
    signed: boolean;
    can_contribute_spec_project: boolean;
  };
  is_committer: boolean;
  friends: {
    friend_id: string;
  };
  first_name: string;
  last_name: string;
  full_name: string;
  github_handle: string;
  twitter_handle: string;
  org: string;
  org_id: number;
  job_title: string;
  website: string;
  country: {
    code: string;
    name: string;
  };
  bio: string;
}

//
// BOTS API
//

export interface BotInstance {
  username: string;
  email: string;
}

export interface BotProperties {
  id: number;
  projectId: string;
  username: string;
  email: string;
}
export type BotDefinition = BotProperties & Record<string, BotInstance>;
