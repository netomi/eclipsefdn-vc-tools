/** ***************************************************************
 Copyright (C) 2022 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

import { GitlabSyncRunner } from './GitlabSyncRunner';
import yargs from 'yargs';
const args = yargs(process.argv)
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('d', {
    alias: 'dryrun',
    description: 'Runs script as dry run, not writing any changes to API',
    boolean: true,
    default: false,
  })
  .option('D', {
    alias: 'devMode',
    description: 'Runs script in dev mode, which returns API data that does not impact production organizations/teams.',
    boolean: true,
    default: false,
  })
  .option('V', {
    alias: 'verbose',
    description: 'Sets the script to run in verbose mode',
    boolean: true,
    default: false,
  })
  .option('H', {
    alias: 'host',
    description: 'GitLab host base URL',
    default: 'http://gitlab.eclipse.org/',
    string: true,
  })
  .option('p', {
    alias: 'provider',
    description: 'The OAuth provider name set in GitLab',
    default: 'oauth2_generic',
    string: true,
  })
  .option('P', {
    alias: 'project',
    description: 'The short project ID of the target for the current sync run',
    string: true,
  })
  .option('r', {
    alias: 'rootGroup',
    description: 'The root group to be used when syncing groups over (e.g. eclipse, openhwgroup)',
    string: true,
    default: 'eclipse',
  })
  .option('s', {
    alias: 'secretLocation',
    description: 'The location of the access-token file containing an API access token',
    string: true,
  })
  .option('S', {
    alias: 'staging',
    description: 'Whether the API should target staging for edge features',
    boolean: true,
  })
  .option('U', {
    alias: 'user',
    description: 'The user running the script',
    string: true,
    default: 'webmaster',
  })
  .help('h')
  .alias('h', 'help')
  .version('0.1')
  .alias('v', 'version')
  .epilog('Copyright 2019 Eclipse Foundation inc.').argv;
run();

/**
 * runs the sync process using the CLI args as configuration items to pass to the sync runner.
 */
async function run() {
  const argv = await args;
  await new GitlabSyncRunner({
    devMode: argv.D,
    host: argv.H,
    dryRun: argv.d,
    provider: argv.p,
    verbose: argv.V,
    project: argv.P,
    secretLocation: argv.s,
    staging: argv.S,
    user: argv.U,
  }).run();
}
