/** *******************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

import { AccessLevel, GroupSchema, IssueSchema, MemberSchema, ProjectSchema, UserSchema } from '@gitbeaker/core/dist/types/types';
import { Logger } from 'winston';
import { getLogger } from '../../helpers/logger';

// used to make use of default requested based on Got rather than recreating our own
import { Gitlab, Resources } from '@gitbeaker/core';
import { requesterFn } from './AxiosRequester';

interface GitlabWrapperConfig {
  host: string;
  token: string;
  verbose: boolean;
  user: string,
  rootGroup?: string;
}

export class GitlabWrapper {

  config: GitlabWrapperConfig;
  logger: Logger;

  api: Resources.Gitlab;

  // caches to optimize calling
  groupsCache: GroupSchema[] = [];
  projectsCache: ProjectSchema[] = [];
  groupMembersCache: Record<number, MemberSchema[]> = {};
  namedUsersCache: Record<string, UserSchema> = {};
  issuesCache: Omit<IssueSchema, 'epic'>[] = [];

  constructor(config: GitlabWrapperConfig) {
    this.config = Object.assign(
      {
        host: 'http://gitlab.eclipse.org/',
        token: '',
        verbose: false,
        user: 'webmaster',
        rootGroup: 'eclipse',
      },
      config,
    );

    this.api = new Gitlab({
      host: this.config.host,
      token: this.config.token,
      requesterFn: requesterFn,
    });

    this.logger = getLogger(this.config.verbose ? 'debug' : 'info', 'main');
  }

  /**
   * Returns all projects from the cache
   * @returns A promise containing a ProjectShema
   */
  getAllProjects(): ProjectSchema[] {
    if (this.config.verbose) {
      this.logger.debug('GitlabWrapper:getAllProjects');
    }

    return this.projectsCache;
  }

  /**
  * Returns all groups from the cache
  * @returns A promise containing a GroupSchema
  */
  getAllGroups(): GroupSchema[] {
    if (this.config.verbose) {
      this.logger.debug('GitlabWrapper:getAllGroups');
    }

    return this.groupsCache;
  }

  /**
  * Returns all groups from the cache
  * @returns A promise containing a GroupSchema
  */
  getAllIssues(): Omit<IssueSchema, 'epic'>[] {
    if (this.config.verbose) {
      this.logger.debug('GitlabWrapper:getAllIssues');
    }

    return this.issuesCache;
  }

  /**
   * Creates a new issue on the given project with the provided options as properties.
   * Returns the new issue or null if an error occured
   * @param projectId the given project id
   * @param options the options including title and description
   * @returns A promise containing the new issue or null
   */
  async createIssue(projectId: number, options: { title: string, description: string }): Promise<IssueSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:createIssue(projectId='${projectId}', options='${options}')`);
    }

    return this.api.Issues.create(projectId, options)
      .then(issue => {
        this.issuesCache.push(issue);
        return issue;
      })
      .catch(err => {
        if (this.config.verbose) {
          this.logger.error(`${err}`);
        }
        return null;
      });
  }

  /**
   * Gets all members for a given project
   * @param projectId The given project Id
   * @param includeInherited The flag to include inherited projects, defaults to false
   * @returns A Promise containing an array of type MemberSchema
   */
  async getAllProjectMembers(projectId: string | number, includeInherited = false): Promise<MemberSchema[]> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:getAllProjectMembers(projectId='${projectId}', includeInherited='${includeInherited}')`);
    }

    return this.api.ProjectMembers.all(projectId, { includeInherited: includeInherited });
  }

  /**
   * Removes a user from a given group using the user id
   * @param groupId The group id
   * @param userId the id of the user to remove
   */
  async removeGroupmember(groupId: string | number, userId: number): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:removeGroupmember(groupId='${groupId}', userId='${userId}')`);
    }

    await this.api.GroupMembers.remove(groupId, userId)
      .catch(err => {
        throw err;
      });
  }

  /**
   * Removes a user from a given project using the user id.
   * @param projectId The project id
   * @param userId The id of the user to remove
   */
  async removeProjectMember(projectId: string | number, userId: number): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:removeProjectMember(groupId='${projectId}', userId='${userId}')`);
    }

    await this.api.ProjectMembers.remove(projectId, userId)
      .catch(err => {
        throw err;
      });
  }

  /**
   * Creates a new user with the provided options as properties. Returns the new user or null if an error occured.
   * Updates the named users cache with the newly created user.
   * @param options the new user properties
   * @returns A promise containing the new user or null
   */
  async createUser(options: {
    username: string;
    password: string;
    force_random_password: boolean;
    name: string;
    email: string;
    extern_uid: string;
    provider: string;
    skip_confirmation: boolean;
  }): Promise<UserSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:createUser(options='${options}')`);
    }

    return this.api.Users.create(options)
      .then(user => {
        this.namedUsersCache[options.name] = user;
        return user;
      })
      .catch(err => {
        if (this.config.verbose) {
          this.logger.error(`${err}`);
        }
        return null;
      });
  }

  /**
   * Creates a new group with the given name, path, and property values. Returns the new group or null if an error occured
   * @param name the group name
   * @param path The group path
   * @param options The group's properties
   * @returns A promise containg the new group or null
   */
  async createGroup(name: string, path: string, options: {
    project_creation_level: string;
    visibility: string;
    request_access_enabled: boolean;
    parent_id: number;
  }): Promise<GroupSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:createGroup(name='${name}', path='${path}', options='${options}')`);
    }

    return this.api.Groups.create(name, path, options)
      .then(group => {
        this.groupsCache.push(group);
        return group;
      })
      .catch(err => {
        if (this.config.verbose) {
          this.logger.error(`${err}`);
        }
        return null;
      });
  }

  /**
   * Returns a UserSchema object from the cache using the cache key.
   * @param userName The cache key
   * @returns The desired userSchema
   */
  getNamedUser(userName: string): UserSchema {
    return this.namedUsersCache[userName];
  }

  /**
   * Retrieves an array of group members from the cache. If the group members do not exist,
   * will attempt to fetch the members from GL. Returns null if an error occurs
   * @param groupId the cache key/group id for querying
   * @param includeInherited A flag to include inherited groups
   * @returns A promise containing a member schema or null
   */
  async getGroupMembers(groupId: number, includeInherited = false): Promise<MemberSchema[] | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:getGroupMembers(groupId='${groupId}')`);
    }

    let members = this.groupMembersCache[groupId];

    // Fetch if not in cache
    if (members === undefined) {
      members = await this.api.GroupMembers.all(groupId, { includeInherited: includeInherited })
        .catch(err => {
          if (this.config.verbose) {
            this.logger.error(`${err}`);
          }
          return null;
        });

      // Set cache with new member list
      this.groupMembersCache[groupId] = members;
    }

    return members;
  }

  /**
   * Edits a group members access level using the group and user ids.
   * @param groupId The group id
   * @param userId The user id
   * @param accessLevel The users access level within the group
   * @returns A promise contaning the editted user or null if an error occured
   */
  async editGroupMember(groupId: string | number, userId: number, accessLevel: AccessLevel): Promise<MemberSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:editGroupMember(groupId='${groupId}', userId='${userId}', accessLevel='${accessLevel}')`);
    }

    return this.api.GroupMembers.edit(groupId, userId, accessLevel)
      .then(members => {
        this.groupMembersCache[groupId] = members;
        return members;
      })
      .catch(err => {
        if (this.config.verbose) {
          this.logger.error(`${err}`);
        }
        return null;
      });
  }

  /**
   * Adds a user to group using the group and user ids. Sets the access level.
   * @param groupId The group id
   * @param userId The user id
   * @param accessLevel The user's access level within the group
   * @returns A promise containing the new user or null if an error occured
   */
  async addGroupMember(groupId: string | number, userId: number, accessLevel: AccessLevel): Promise<MemberSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabWrapper:addGroupMember(groupId='${groupId}', userId='${userId}', accessLevel='${accessLevel}')`);
    }

    return this.api.GroupMembers.add(groupId, userId, accessLevel)
      .then(members => {
        this.groupMembersCache[groupId] = members;
        return members;
      })
      .catch(err => {
        if (this.config.verbose) {
          this.logger.error(`${err}`);
        }
        return null;
      });
  }

  /**
   * Pre-loads all caches
   */
  async prepareCaches(): Promise<void> {
    try {
      this.logger.info('Loading Gitlab projects');

      this.logger.info('Populating Gitlab projects cache');
      this.projectsCache = await this.api.Projects.all();

      this.logger.info('Populating Gitlab groups cache');
      this.groupsCache = await this.api.Groups.all();

      this.logger.info('Populating Issues cache');
      this.issuesCache = Array.from(await this.api.Issues.all({ author_username: this.config.user }));

      this.logger.info('Populating Gitlab users cache');
      const users = await this.api.Users.all();

      this.logger.info('Mapping Gitlab users');
      this.namedUsersCache = users.reduce((acc, item) => ({ ...acc, [item.username]: item }), {} as Record<string, UserSchema>);
    } catch (e) {
      this.logger.error(e);
      this.logger.error(`Cannot fetch resources associated with sync operations, exiting: ${e}`);
      process.exit(1);
    }
  }
}
