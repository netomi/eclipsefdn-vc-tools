/**
 * Original source: https://github.com/jdalrymple/gitbeaker/blob/6a3292b03342326a98b595a70ab26eb12ddb7ba4/packages/node/src/GotRequester.ts
 *
 * Source modified to reduce rewrite where possible to implement a layer using axios which is our
 * known+used HTTP wrapper impl. Changes include mapping Got params to axios params, and removing the
 * `delay` package dependency as there is a more typescript-y way to do it.
 */

import axios from 'axios';
import { decamelizeKeys } from 'xcase';
import {
  DefaultResourceOptions,
  DefaultRequestReturn,
  DefaultRequestOptions,
  createRequesterFn,
  defaultOptionsHandler as baseOptionsHandler,
} from '@gitbeaker/requester-utils';
const tooManyRequestsStatus = 429;
const badGatewayStatus = 502;
const retryCodes = [tooManyRequestsStatus, badGatewayStatus];
const waitTimeBase = 2;

export function defaultOptionsHandler(
  resourceOptions: DefaultResourceOptions,
  { body, query, sudo, method }: DefaultRequestOptions = {},
): DefaultRequestReturn & {
  json?: Record<string, unknown>;
  https?: { rejectUnauthorized: boolean };
} {
  const options: DefaultRequestReturn & {
    json?: Record<string, unknown>;
    https?: { rejectUnauthorized: boolean };
  } = baseOptionsHandler(resourceOptions, { body, query, sudo, method });

  // FIXME: Not the best comparison, but...it will have to do for now.
  if (typeof body === 'object' && body.constructor.name !== 'FormData') {
    options.json = decamelizeKeys(body);

    delete options.body;
  }

  if (resourceOptions.url.includes('https') && resourceOptions.rejectUnauthorized != null && resourceOptions.rejectUnauthorized === false) {
    options.https = {
      rejectUnauthorized: resourceOptions.rejectUnauthorized,
    };
  }

  return options;
}

export function processBody({ data, headers }: { data: Buffer; headers: Record<string, unknown> }) {
  // Split to remove potential charset info from the content type
  const contentType = ((headers['content-type'] as string) || '').split(';')[0].trim();

  if (contentType === 'application/json') {
    return data.length === 0 ? {} : data;
  }

  if (contentType.startsWith('text/')) {
    return data.toString();
  }

  return Buffer.from(data);
}

export async function handler(endpoint: string, options: Record<string, unknown>) {
  const maxRetries = 10;
  let response;
  // map the Got prefix to the axios base url
  options['baseURL'] = options['prefixUrl'];
  // map the qs glob into an object
  options['params'] = Object.fromEntries(new URLSearchParams(options['searchParams'] as string));
  options['data'] = options['json'];
  delete options['json'];
  for (let i = 0; i < maxRetries; i += 1) {
    const waitTime = waitTimeBase ** i;

    try {
      response = await axios(endpoint, { ...options }); // eslint-disable-line
      break;
    } catch (e) {
      if (e.response) {
        if (retryCodes.includes(e.response.statusCode)) {
          await new Promise(resolve => setTimeout(resolve, waitTime));
          continue; // eslint-disable-line
        }

        if (typeof e.response.body === 'string' && e.response.body.length > 0) {
          try {
            const output = JSON.parse(e.response.body);
            e.description = output.error || output.message;
          } catch (err) {
            e.description = e.response.body;
          }
        }
      }

      throw e;
    }
  }
  const { status, headers } = response;
  const body = processBody(response);
  return { body, headers, status: status };
}

export const requesterFn = createRequesterFn(defaultOptionsHandler, handler);
