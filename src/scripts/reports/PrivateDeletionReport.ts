/** *****************************************************************************
 * Copyright (C) 2022 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  - Martin Lowe <martin.lowe@eclipse-foundation.org>
 *  - Zachary Sabourin <zachary.sabourin@eclipse-foudnation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

import { Logger } from 'winston';
import axios from 'axios';
import parse from 'parse-link-header';
import { Gitlab } from '@gitbeaker/core';
import { requesterFn } from '../gl/AxiosRequester';

import { SecretReader, getBaseConfig } from '../../helpers/SecretReader';
import { getLogger } from '../../helpers/logger';

import { ProjectExtendedSchema } from '@gitbeaker/core/dist/types/types';

import yargs from 'yargs';
import { exit } from 'process';
const args = yargs(process.argv)
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('H', {
    alias: 'host',
    description: 'GitLab host base URL',
    default: 'http://gitlab.eclipse.org/',
    string: true,
  })
  .option('p', {
    alias: 'provider',
    description: 'The OAuth provider name set in GitLab',
    default: 'oauth2_generic',
    string: true,
  })
  .option('s', {
    alias: 'secretLocation',
    description: 'The location of the access-token file containing an API access token',
    string: true,
  })
  .help('h')
  .alias('h', 'help')
  .version('0.1')
  .alias('v', 'version')
  .epilog('Copyright 2022 Eclipse Foundation inc.').argv;

/**
 * Used for private repo expiry
 */
const repoExpiryDays = 90;
const hoursInDay = 24;
const minsInHour = 60;
const secondsInMinute = 60;
const millisInSecond = 1000;
const ninetyDaysAsMillis = repoExpiryDays * hoursInDay * minsInHour * secondsInMinute * millisInSecond;

/**
 * Explicit config wrapper. Contains necessary refs to run the report.
 */
export interface DeletionReportConfig {
  host: string;
  secretLocation: string;
  provider: string;
}

/**
 * Report processing class. Instantiated to hold secrets and run the report building, and spit out to console at the end.
 */
export class PrivateDeletionReport {
  logger: Logger;
  config: DeletionReportConfig;
  accessToken: string;
  api: Gitlab;

  constructor(config: DeletionReportConfig) {
    this.config = config;
    this.logger = getLogger('debug', 'main');
    this._prepareSecret();
    this.api = new Gitlab({
      host: this.config.host,
      token: this.accessToken,
      requesterFn: requesterFn,
    });
  }

  /**
   * Prepares the secrets required for the script to run. Specifically the eclipseToken used for Eclipse
   * API access and the accessToken which is used for sudo+api access on the Gitlab instance targeted by
   * this script.
   */
  _prepareSecret() {
    // retrieve the secret API file root if set
    const settings = getBaseConfig();
    if (this.config.secretLocation !== undefined) {
      settings.root = this.config.secretLocation;
    }
    const reader = new SecretReader(settings);
    const data = reader.readSecret('access-token');
    if (data !== null) {
      this.accessToken = data.trim();
    } else {
      this.logger.error('Could not find the GitLab access token, exiting');
      process.exit(1);
    }
  }

  /**
   * Actually run the report. Will fetch private projects, iterate over them and filter out some projects that aren't (yet)
   * subject to deletion. Will then format the data and fetch the associated user for the project so that we can contact them if needed.
   */
  async run(): Promise<void> {
    this.logger.debug('DeletionReport:run');
    const projects = await this.fetchPrivateProjects();
    if (projects === null) {
      exit(1);
    }
    const targetDate = Date.now() - ninetyDaysAsMillis;
    this.logger.info(`Looking for dates before ${targetDate}`);
    const out: Record<string, unknown>[] = [];
    // check that active date is farther than 90d, and also only in EF project namespace
    const filteredProjects = projects
      .filter(p => targetDate > Date.parse(p.last_activity_at))
      .filter(p => p.owner !== undefined || p.path_with_namespace.startsWith('eclipse/'));
    // logic not inline as async requirements to fetch users breaks Arrays.map() func
    for (const idx in filteredProjects) {
      const p = filteredProjects[idx];
      out.push({
        name: p.name,
        path: p.path_with_namespace,
        last_active: p.last_activity_at,
        creator: await this.mapCreatorIdToUser(p.creator_id),
      });
    }
    // display report JSON to console
    this.logger.info(JSON.stringify(out));
  }

  /**
   * Retrieves the user associated with the creator of a project.
   *
   * @param creatorId the user ID of the project creator.
   * @returns the users name + username to facilitate contact if needed.
   */
  async mapCreatorIdToUser(creatorId: number): Promise<Record<string, string>> {
    const user = await this.api.Users.show(creatorId);
    return {
      name: user.name,
      username: user.username,
    };
  }

  /**
   * Fetches private projects from the gitlab api using axios. Returns null if there was an error.
   * Using axios since the gitbreaker projects.all() does not return the ProjectExtendedSchema
   * required to obtain the 'visibility' property.
   *
   * @returns ProjectExtendedShema array or null
   */
  async fetchPrivateProjects(): Promise<ProjectExtendedSchema[] | null> {
    try {
      this.logger.debug('DeletionReport:fetchPrivateProjects');
      let results: ProjectExtendedSchema[] = [];
      let hasMoreResults = true;
      let url = this.config.host.concat('/api/v4/projects?simple=no&visibility=private&per_page=100');

      while (hasMoreResults) {
        const response = await axios.get(url, {
          headers: { 'PRIVATE-TOKEN': this.accessToken },
        });

        results = results.concat(response.data as ProjectExtendedSchema[]);

        // Parse link headers and check for next URL
        const linkHeaders = parse(response.headers['link']);
        if (linkHeaders.next !== undefined) {
          url = linkHeaders.next.url;
        } else {
          hasMoreResults = false;
        }
      }

      return results;
    } catch (err) {
      this.logger.error(`Error while fetching private projects: ${err}`);
      return null;
    }
  }
}
// do the thing
const run = async args => {
  const argv = await args;
  new PrivateDeletionReport({ host: argv.H, secretLocation: argv.s, provider: argv.p }).run();
};
run(args);
