/** *****************************************************************************
 * Copyright (C) 2019 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

// custom wrappers
const Wrapper = require('./GitWrapper.js');
const { HttpWrapper } = require('./HttpWrapper.js');
const EclipseAPI = require('./EclipseAPI.js');

// set up yargs command line parsing
const argv = require('yargs')
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('d', {
    alias: 'dryrun',
    description: 'Runs script as dry run, not writing any changes to API',
    boolean: true,
  })
  .option('T', {
    alias: 'target',
    description: 'Target organization(s) to update',
  })
  .option('i', {
    alias: 'inverse',
    description: 'Inverse the target argument to exclude targeted groups',
    boolean: true,
  })
  .option('t', {
    alias: 'devMode',
    description: 'Runs script in dev mode, which returns API data that does not impact production organizations/teams. '
      + 'This does NOT affect the static team manager. If testing is wanted for that integration, verbose and dryrun mode are suggested.',
    boolean: true,
  })
  .option('V', {
    alias: 'verbose',
    description: 'Sets the script to run in verbose mode',
    boolean: true,
  })
  .option('D', {
    alias: 'deletionDryRun',
    description: 'Runs the script in a semi-dryrun state to prevent deletions of users',
    boolean: true,
  })
  .option('p', {
    alias: 'project',
    description: 'The project ID that should be targeted for this sync run',
  })
  .option('s', {
    alias: 'secretLocation',
    description: 'The location of the access-token file containing an API access token',
  })
  .help('h')
  .alias('h', 'help')
  .version('0.1')
  .alias('v', 'version')
  .epilog('Copyright 2019 Eclipse Foundation inc.')
  .argv;

const DEFAULT_ORG_PERMISSIONS = {
  default_repository_permission: 'none',
  members_can_create_repositories: false,
  members_can_create_private_repositories: false,
  members_can_create_public_repositories: false,
  members_allowed_repository_creation_type: 'none',
};
const DENYLIST_ORGS_ORG_WIDE_PROJECT = ['eclipse'];
const DENYLIST_ORG_READ_UPDATES = ['openhwgroup'];
const DENYLIST_ORG_PERM_UPDATES = [];
const HTTP_NOT_FOUND_STATUS = 404;
const SECURITY_REPO_NAME = '.eclipsefdn';
const INTERNAL_TEAM_NAMES = ['eclipsefdn-security', 'eclipsefdn-releng'];

const { getLogger } = require('./logger.js');
const logger = getLogger(argv.V ? 'debug' : 'info', 'main');
const axios = require('axios');
const { SecretReader, getBaseConfig } = require('./SecretReader.js');
const { StaticTeamManager, ServiceTypes } = require('./teams/StaticTeamManager.js');

// create global placeholder for wrapper
let wrap;
let cHttp;
let eclipseApi;
let bots;
let stm;

_prepareSecret();

/**
 * Retrieves secret API token from system, and then starts the script via _init
 *
 * @returns
 */
function _prepareSecret() {
  // retrieve the secret API file root if set
  const settings = getBaseConfig();
  if (argv.s !== undefined) {
    settings.root = argv.s;
  }
  const reader = new SecretReader(settings);
  // get the secret and start the script if set
  const secret = reader.readSecret('api-token');
  if (secret !== null) {
    _init(secret.trim());
  }
}

/**
 * Async as we require blocking to ensure that data is available when processing
 * starts.
 */
async function _init(secret) {
  if (secret === undefined || secret === '') {
    logger.error('Could not fetch API secret, exiting');
    return;
  }
  wrap = new Wrapper(secret);
  if (!await wrap.checkAccess()) {
    return;
  }
  wrap.setDryRun(argv.d);
  wrap.setVerbose(argv.V);
  logger.info(`Running in dryrun? ${argv.d}`);

  cHttp = new HttpWrapper();
  stm = new StaticTeamManager();
  stm.verbose = argv.V;

  eclipseApi = new EclipseAPI();
  eclipseApi.verbose = argv.V;
  eclipseApi.testMode = argv.t;
  // get raw project data and post process to add additional context
  let data = await eclipseApi.eclipseAPI('?github_only=1');
  data = eclipseApi.postprocessEclipseData(data, 'github_repos');

  logger.info(`Finished preloading ${data.length} projects`);
  // get bots for raw project processing
  const rawBots = await eclipseApi.eclipseBots();
  bots = eclipseApi.processBots(rawBots);
  logger.info(`Found ${Object.keys(bots).length} registered bots`);

  // start the sync operation.
  await runSync(data);

  // close the wrappers, persisting required cache info
  cHttp.close();
}

async function runSync(data) {
  const start = new Date();
  // used to track which orgs have been processed for removing outside collabs
  const uniqueOrgs = [];
  for (const key in data) {
    const project = data[key];

    // check that the project matches the project target if set
    const projectID = project.project_id;
    if (argv.p !== undefined && projectID !== argv.p) {
      console.log(`Project target set ('${argv.p}'). Skipping non-matching project ${projectID}`);
      continue;
    }
    logger.info(`Project ID: ${projectID}`);

    // Retrieve both distributed and organization wide project repos
    let repos = await handleDistributedProjects(project);
    const orgWideRepos = await handleOrgWideProject(project);
    if (orgWideRepos !== null) {
      repos = repos.concat(orgWideRepos);
    }
    // deduplicate repos, find index will eject any entries that don't match the first index of a unique ID
    repos = repos.filter((p1, i, a) => a.findIndex(p2 => p2.repo.localeCompare(p1.repo, undefined, { sensitivity: 'base' }) === 0) === i);

    // updates the orgs ahead of time by grabbing unique
    const projectOrgs = repos
      .map(r => r.org)
      .filter((orgName, index, originalArray) => originalArray.indexOf(orgName) === index);
    // if empty array, then no repos could be found but org is valid and should be included
    if (orgWideRepos !== null && orgWideRepos.length === 0) {
      projectOrgs.push(project.github.org);
    }
    // process each of the orgs, adding teams and cleaning outside collaborators
    for (let orgIdx in projectOrgs) {
      const org = projectOrgs[orgIdx];
      logger.info(`Generating teams for ${org} for project ${projectID}`);
      // process the org to add teams
      await processProjectsOrg(org, project);

      // remove external contributors as well
      if (!uniqueOrgs.includes(org)) {
        logger.info(`Removing outside collaborators for ${org}`);
        await removeOrgExternalContributors(data, org);
        uniqueOrgs.push(org);
      }
    }

    // process the repositories for this project using the teams generated earlier
    await processRepositories(repos, project);
  }

  logger.info('Beginning processing of static teams');
  // retrieve the static teams for GitHub
  const teams = stm.processTeams(ServiceTypes.GITHUB);
  if (argv.V) {
    logger.info(`Number of custom teams discovered: ${teams.length}`);
  }
  for (const tIdx in teams) {
    const team = teams[tIdx];
    // process each team individually
    await processStaticTeam(team, uniqueOrgs);
  }

  // log how long it took to do this stuff
  const end = new Date();
  logger.info(`Start: ${start}, end: ${end}, calls: ${wrap.getCallCount()}`);
}

/**
 * Retrieves and formats repos from organization into simplified format for use in repository processing in later
 * stages. These orgs are unfiltered and should represent all repos within an org.
 */
async function handleOrgWideProject(project) {
  // check that this code should be used in this project
  const projectOrg = project.github.org;
  if (projectOrg === undefined || projectOrg.trim() === ''
      || DENYLIST_ORGS_ORG_WIDE_PROJECT.includes(projectOrg.trim().toLowerCase())) {
    logger.info(`Project ${project.project_id} does not have a valid Github project org set, not using org wide logic`);
    return null;
  }

  // check if the designated org is available for current run
  if (argv.T && projectOrg.trim().localeCompare(argv.T) !== 0) {
    logger.info(`Project ${project.project_id} is outside of target org ${argv.T} and will be skipped.`);
    return null;
  }

  // get the repos from Github for current organization and convert into simplified format
  const orgRepos = await wrap.getReposForOrg(projectOrg.trim().toLowerCase());
  if (orgRepos === undefined || orgRepos.length === 0) {
    logger.info(`Error while retrieving repos associated with org ${projectOrg.trim().toLowerCase()}`);
    return [];
  }

  // if we can find repos, filter out the ignored repos
  logger.info(`Found ${orgRepos.length} potential repos using org-wide retrieval logic`);
  return orgRepos.filter(r => project.github.ignored_repos.indexOf(r.name) === -1
      && r.name.localeCompare(SECURITY_REPO_NAME) !== 0).map(r => {
    return {
      org: projectOrg,
      repo: r.name,
    };
  });
}

/**
 * Retrieves and filters repos based on the organization target in the arguments without further mutating the EF provided project data.
 */
async function handleDistributedProjects(project) {
  let repos = project.github_repos;
  // if org target set, then filter the projects
  if (argv.T) {
    repos = repos.filter(v => argv.i ? v.org !== argv.T : v.org === argv.T);
    logger.info(`Filtered ${project.github_repos.length} repos to ${repos.length} repos after applying target ${argv.T} `
      + `(inverse? ${argv.i})`);
  }
  return repos;
}

async function processRepositories(repos, project) {
  if (argv.V === true) {
    logger.debug(`Sync:processRepositories(repos = ${JSON.stringify(repos)}, project = ${project.project_id})`);
  }
  for (let idx in repos) {
    const repo = repos[idx];
    const org = repo.org;
    const repoName = repo.repo;
    logger.info(`Starting sync for org=${org};repo=${repoName}`);

    // process contributors for the team
    const successfullyUpdatedExt = await removeRepoExternalContributors(project, org, repoName);
    if (argv.V === true) {
      logger.verbose(`Updated external contributors: ${successfullyUpdatedExt}`);
    }
    if (!argv.d) {
      try {
        // Ensure that the teams refer to the repo
        const updatedCommit = await wrap.addRepoToTeam(org, `${project.project_id}-committers`, repoName, 'push');
        const updatedContrib = await wrap.addRepoToTeam(org, `${project.project_id}-contributors`, repoName, 'triage');
        const updatedPL = await wrap.addRepoToTeam(org, `${project.project_id}-project-leads`, repoName, 'maintain', false);
        if (argv.V === true) {
          logger.verbose(`Attempted update commit team: ${updatedCommit === undefined}`);
          logger.verbose(`Attempted update contrib team: ${updatedContrib === undefined}`);
          logger.verbose(`Attempted update pl team: ${updatedPL === undefined}`);
        }
      } catch (e) {
        logger.error(`Error while updating ${project.project_id}. \n${e}`);
      }
    } else {
      logger.debug(`Dry run set, not adding repo '${repoName}' for org: ${org}`);
    }
  }
}

/**
 * Accepts a team and the list of unique organizations affected by this sync run. The unique orgs will be used to add
 * service type team to orgs when present.
 */
async function processStaticTeam(team, uniqueOrgs) {
  const orgs = [];
  logger.info(`Processing static team ${team.name}`);
  // check if we are processing a service team or a standard static team
  if (team.serviceTeamFor !== '' && uniqueOrgs !== undefined && uniqueOrgs.length > 0) {
    logger.info(`Processing team '${team.name}' for ${uniqueOrgs.length} organizations`);
    // iterate over the unique orgs and add the static team to them
    for (const oIdx in uniqueOrgs) {
      // get the current org
      const org = uniqueOrgs[oIdx];
      // check expiration and then add the team to the org
      if ((await checkStaticTeamExpiration(team, org)) === true) {
        logger.info(`Static team '${team.name}' has expired, has been removed from the organization ${org}, and will not be active`);
        continue;
      }
      logger.info(`Generating static team '${team.name}' for organization ${org}`);
      await processOrg(org, team);

      // special logic for private foundation static teams
      if (INTERNAL_TEAM_NAMES.includes(team.name)) {
        logger.info(`Attempting to add ${team.name} team to ${SECURITY_REPO_NAME} for org ${org}`);
        await wrap.addRepoToTeam(org, team.name, SECURITY_REPO_NAME, 'push');
      }
    }
  } else {
    for (const rIdx in team.repos) {
      const repoURL = team.repos[rIdx];
      const match = /\/([^/]+)\/([^/]+)\/?$/.exec(repoURL);
      // check to make sure we got a match
      if (match == null) {
        logger.warn(`Cannot match repo and org from repo URL ${repoURL}, skipping`);
        continue;
      }

      // get the org + repo from the repo URL
      const org = match[1];
      const repoName = match[2];
      if (argv.V) {
        logger.info(`Processing static team ${team.name} for repo ${repoName} in org ${org}`);
      }
      // check expiration and then add the team to the org
      if ((await checkStaticTeamExpiration(team, org)) === true) {
        logger.info(`Static team '${team.name}' has expired, has been removed from the organization ${org}, and will not be active`);
        continue;
      }
      if (!orgs.includes(org)) {
        logger.info(`Generating teams for ${org}/${repoName}`);
        await processOrg(org, team);
        orgs.push(org);
      }
      if (!argv.d) {
        try {
          // update the team to have access to the repository
          await wrap.addRepoToTeam(org, wrap.sanitizeTeamName(team.name), repoName, team.permission);
        } catch (e) {
          logger.warn(`Error while updating ${wrap.sanitizeTeamName(team.name)}. \n${e}`);
        }
      } else {
        logger.debug(`Dry run set, not adding repo '${repoName}' to team '${wrap.sanitizeTeamName(team.name)}' for org: ${org}`);
      }
    }
  }
}

/**
 * Checks if a static team should be removed from the given organization using the expiration date.
 */
async function checkStaticTeamExpiration(team, org) {
  // check if team is expired and should be deleted/skipped
  if (team.expiration !== undefined) {
    const expirationDate = new Date(team.expiration);
    // check if the expiration value is valid and after now
    if (expirationDate.getTime() < Date.now()) {
      logger.info(`Team with name ${team.name} is expired, it will be removed if present`);
      await wrap.removeTeam(org, wrap.sanitizeTeamName(team.name));
      return true;
    }
  }
  return false;
}

async function processProjectsOrg(org, project) {
  if (argv.V === true) {
    logger.debug(`Sync:processProjectsOrg(org = ${org}, project = ${project.project_id})`);
  }
  // prefetch teams to reduce redundant calls
  await Promise.allSettled([wrap.prefetchTeams(org), wrap.prefetchRepos(org)]);

  // create the teams for the current org + update perms
  if (!argv.d) {
    // split out org updates to allow optional operations on orgs
    let orgUpdates = [
      updateProjectTeam(org, project, 'contributors'),
      updateProjectTeam(org, project, 'committers'),
      updateProjectTeam(org, project, 'project_leads')];

    // check deny lists before adding the org permission updates
    if (DENYLIST_ORG_PERM_UPDATES.includes(org)) {
      logger.info(`Skipping updating permissions for org ${org} as it has been denylisted`);
    } else if (DENYLIST_ORG_READ_UPDATES.includes(org)) {
      logger.info(`Org ${org} will be updated with modified settings (read access default)`);
      orgUpdates.push(wrap.updateOrgPermissions(org, {...DEFAULT_ORG_PERMISSIONS, default_repository_permission: 'read'}));
    } else {
      orgUpdates.push(wrap.updateOrgPermissions(org, DEFAULT_ORG_PERMISSIONS));
    }

    const done = await Promise.allSettled(orgUpdates);
    logger.debug(`Finished creating teams for ${project.project_id} in org '${org}' with `
      + `${done.filter(status => status.status !== 'fulfilled').length} errors.`);

    // add the committers team to the private foundation team if it exists
    const updatedPrivateFdn = await Promise.allSettled([
      wrap.addRepoToTeam(org, wrap.sanitizeTeamName(`${project.project_id}-project-leads`), SECURITY_REPO_NAME, 'triage'),
      wrap.addRepoToTeam(org, wrap.sanitizeTeamName(`${project.project_id}-committers`), SECURITY_REPO_NAME, 'pull'),
    ]);
    logger.verbose(
      `${updatedPrivateFdn.filter(status => status.status !== 'fulfilled').length} errors updating committer + PL team (Private FDN team)`
    );
  } else {
    logger.debug('Dry run set, not adding teams for org: ' + org);
  }
}
async function processOrg(org, team) {
  if (argv.V === true) {
    logger.debug(`Sync:processOrg(org = ${org}, team = ${team})`);
  }
  // prefetch teams to reduce redundant calls
  await wrap.prefetchTeams(org);
  await wrap.prefetchRepos(org);
  const teamName = wrap.sanitizeTeamName(team.name);
  // create the teams for the current org
  if (!argv.d) {
    await updateTeam(org, { teamName: teamName, privacy: team.privacy, description: team.description }, team.members, undefined);
  } else {
    logger.debug(`Dry run set, not adding team '${teamName}' for org: ${org}`);
    if (argv.V) {
      logger.silly(`Would have added the following users to team '${teamName}': \n${JSON.stringify(team.members)}`);
    }
  }
}

async function updateProjectTeam(org, project, grouping) {
  if (argv.V === true) {
    logger.debug(`Sync:updateProjectTeam(org = ${org}, project = ${project.project_id}, grouping = ${grouping})`);
  }
  const projectID = project.project_id;
  const teamName = wrap.sanitizeTeamName(`${projectID}-${grouping}`);
  await updateTeam(org, { teamName: teamName, privacy: 'secret' }, project[grouping], project);
}

async function updateTeam(org, teamProperties, designatedMembers, project) {
  const teamName = teamProperties.teamName;
  if (argv.V === true) {
    logger.debug(`Sync:updateTeam(org = ${org}, teamName = ${teamName}, designatedMembers = ${JSON.stringify(designatedMembers)})`);
  }
  logger.info(`Syncing team '${teamName}' for organization ${org}`);
  const team = await wrap.addTeam(org, teamName);
  if (team === undefined) {
    logger.warn(`Unable to create or find team '${teamName}', will not continue with updating team`);
    return;
  }

  // set team to private
  await wrap.editTeam(org, teamName, { privacy: teamProperties.privacy || 'secret', description: teamProperties.description || ''});
  let members = await wrap.getTeamMembers(org, team);

  let shouldRemoveUsers = true;
  logger.silly(`${teamName} members: ${JSON.stringify(designatedMembers)}`);
  for (const idx in designatedMembers) {
    // check if member has an expiration value set
    if (designatedMembers[idx].expiration !== undefined) {
      const expirationDate = new Date(designatedMembers[idx].expiration);
      // check if the expiration value is valid and after now
      if (expirationDate.getTime() < Date.now()) {
        continue;
      }
    }

    // get the user via cached HTTP
    const userRequest = await cHttp.getRaw(designatedMembers[idx].url);
    if (userRequest instanceof Error) {
      if (!checkIfUserIsMissing(userRequest.response)) {
        logger.error(`Error while fetching user data at ${designatedMembers[idx].url}, removal will be skipped for this team for this run`);
        shouldRemoveUsers = false;
      } else {
        logger.error(`No user data could be retrieved for ${designatedMembers[idx].url}`);
      }
    } else {
      const user = userRequest.data;
      // check if github handle is null or empty
      if (!user.github_handle || user.github_handle.trim() === '') {
        logger.verbose(`User '${designatedMembers[idx].name}' has no associated GitHub username, skipping`);
        continue;
      }

      // invite user to team
      await wrap.inviteUserToTeam(org, teamName, user.github_handle);
      if (members !== undefined) {
        // remove just the user that matches the username
        members = members.filter(e => e.login.localeCompare(user.github_handle, undefined, { sensitivity: 'base' }));
      }
    }
  }

  logger.silly(`Leftover members: ${JSON.stringify(members)}`);

  // for each left over member, check if its a bot
  if (members !== undefined) {
    if (shouldRemoveUsers) {
      members.forEach(async member => {
        // bot check before deletion, skipping if user is bot
        if (!isUserBot(member.login, project)) {
          if (argv.D !== true) {
            logger.info(`Removing '${member.login}' from team '${teamName}'`);
            await wrap.removeUserFromTeam(org, teamName, member.login);
          } else {
            logger.debug(`Would have deleted '${member.login}', but in semi-dry run mode`);
          }
        } else {
          logger.verbose(`User '${member.login}' from team '${teamName}' identified as a bot, skipping`);
        }
      });
    } else {
      logger.info('An error encountered while processing team members, users will not be removed from this team on this run');
    }
  }
}

/**
 * Checks if the response from upstream indicates a missing user rather than another error. This will check the following
 * data points to confirm user is missing rather than server problems:
 *
 * - There was a response from the server
 * - Status is set to 404
 * - Response is an array that contains a single string indicating "User not found."
 *
 * @param response the Axios errors response property.
 * returns true if the user is missing, false if any other kind of error
 */
function checkIfUserIsMissing(response) {
  return response != null && response.status === HTTP_NOT_FOUND_STATUS && response.data instanceof Array
      && response.data[0].localeCompare('User not found.', undefined, { sensitivity: 'base' }) === 0;
}

async function removeRepoExternalContributors(project, org, repo) {
  if (argv.V === true) {
    logger.debug(`Sync:removeRepoExternalContributors(project = ${project.project_id}, org = ${org}, repo = ${repo})`);
  }
  // get the collaborators
  const collaborators = await wrap.getRepoCollaborators(org, repo);
  if (collaborators === undefined) {
    logger.error(`Error while fetching collaborators for ${org}/${repo}`);
    return false;
  }
  // check if we have collaborators to process
  if (collaborators.length === 0) {
    return false;
  }

  const projBots = bots[project.project_id];
  for (const collabIdx in collaborators) {
    const uname = collaborators[collabIdx].login;
    // skip webmaster
    if (uname === 'eclipsewebmaster') {
      continue;
    }

    // get the bots for the current project
    if (projBots !== undefined && projBots.indexOf(uname) !== -1) {
      logger.verbose(`Keeping ${uname} as it was detected to be a bot for ${org}/${repo}`);
      continue;
    }

    // get the current users profile
    const url = `https://api.eclipse.org/github/profile/${uname}`;
    const r = await axios.get(url).then(result => {
      return result.data;
    }).catch(err => logger.error(`Received error from Eclipse API querying for '${url}': ${err}`));
    // check user against list of project leads
    if (r != null) {
      const eclipseUserName = r.name;
      let isProjectLead = false;
      for (const plIdx in project['project_leads']) {
        const projectLead = project['project_leads'][plIdx];
        if (projectLead.username === eclipseUserName) {
          isProjectLead = true;
          break;
        }
      }
      if (isProjectLead) {
        logger.verbose(`User '${eclipseUserName}' is a project lead for the current repository, not removing`);
        continue;
      }
    }
    // remove collaborator if we've gotten to this point and dryrun isn't set
    if (!argv.d) {
      logger.info(`Removing user '${uname}' from collaborators on ${org}/${repo}`);
      await wrap.removeUserAsCollaborator(org, repo, uname);
    } else {
      logger.verbose(`Dry run set, would have removing user '${uname}' from collaborators on ${org}/${repo}`);
    }
  }
  return true;
}


async function removeOrgExternalContributors(projects, org) {
  if (argv.V === true) {
    logger.debug(`Sync:removeOrgExternalContributors(projects = (${projects.length} projects), org = ${org})`);
  }
  // get the collaborators
  const collaborators = await wrap.getOrgCollaborators(org);
  if (collaborators === undefined) {
    logger.error(`Error while fetching collaborators for ${org}`);
    return;
  }
  // check if we have collaborators to process
  if (collaborators.length === 0) {
    return;
  }
  // check each of the collaborators, removing them if they arent a bot for a
  // project in the org
  for (const collabIdx in collaborators) {
    const uname = collaborators[collabIdx].login;
    logger.verbose(`Checking collaborator '${uname}'...`);

    let isBot = false;
    const botKeys = Object.keys(bots);
    for (const botIdx in botKeys) {
      const botList = bots[botKeys[botIdx]];
      // check if the current user is in the current key-values list
      if (botList.indexOf(uname) !== -1) {
        logger.verbose(`Found user '${uname}' in bot list for project '${botKeys[botIdx]}', checking organizations`);
        // if we can determine that this user could be a bot, check that its
        // valid for current org
        for (const pIdx in projects) {
          const project = projects[pIdx];
          // check if our project ID is the ID associated with bot
          // and if the project has repositories within the given org
          if (project.project_id === botKeys[botIdx] && project.pp_orgs.indexOf(org) !== -1) {
            isBot = true;
            logger.verbose(`Discovered bot account for '${botKeys[botIdx]}' in org ${org}`);
            break;
          }
        }
      }
      // if we flagged the user as a bot, stop processing
      if (isBot) {
        break;
      }
    }
    // check if the user was flagged as a bot for the current org
    if (isBot) {
      logger.verbose(`Keeping '${uname}' as it was detected to be a bot for org '${org}'`);
      continue;
    }

    // remove collaborator if we've gotten to this point and dryrun isn't set
    if (!argv.d) {
      logger.info(`Removing user '${uname}' from collaborators on org '${org}'`);
      await wrap.removeUserAsOutsideCollaborator(org, uname);
    } else {
      logger.verbose(`Dry run set, would have removing user '${uname}' from collaborators on ${org}`);
    }
  }
}

function isUserBot(uname, project) {
  if (project !== undefined) {
    const botList = bots[project.project_id];
    // check if the current user is in the current key-values list for project
    if (botList && botList.indexOf(uname) !== -1) {
      logger.info(`Found user '${uname}' in bot list for project '${project.project_id}'`);
      return true;
    }
  }
  return false;
}
