# eclipsefdn-github-sync

The Eclipse Foundation toolkit for maintaining permissions across multiple version-control platforms. This includes scripts for syncronizing user permissions from PMI to Github or Gitlab, running automated backups of repositories, and managing bot access through a variety of means.

## Table of Contents
<!-- TOC -->
- [Table of Contents](#table-of-contents)
- [Gaining Access to a project](#gaining-access-to-a-project)
- [Github Sync](#github-sync)
    - [Github Permissions mapping](#github-permissions-mapping)
- [Gitlab Sync](#gitlab-sync)
    - [Gitlab Permissions mapping](#gitlab-permissions-mapping)
- [Usage](#usage)
    - [Manual run parameters](#manual-run-parameters)
        - [Github params](#github-params)
        - [Gitlab params](#gitlab-params)
    - [Running the toolset for development](#running-the-toolset-for-development)
        - [Running for Github](#running-for-github)
        - [Running for Gitlab](#running-for-gitlab)
            - [Gitlab Secrets](#gitlab-secrets)
            - [Gitlab script run command](#gitlab-script-run-command)
- [Creating a tagged release](#creating-a-tagged-release)
- [Maintainers](#maintainers)
- [Trademarks](#trademarks)
- [Copyright and license](#copyright-and-license)
<!-- /TOC -->

## Gaining Access to a project

To be granted access to a repository through the Eclipse Sync scripts (Gitlab or Github), a user must first either be nominated as a committer or a project lead within the [PMI(projects management infrastructure)](https://wiki.eclipse.org/Project_Management_Infrastructure) or be added as a contributor by an active committer or project lead. Depending on the role granted within the PMI, different access rights will be granted through the sync scripts. Should a user be promoted or retired within a project, the new permission sets should be active within a few hours of finalization.

Bot access within repositories is possible, but managed by a manual process and tracked by a [publicly available API](https://api.eclipse.org/bots) rather than through the sync script. How these bot permissions are typically interpreted varies by platform, and more info for each is available in the Github and Gitlab Sync sections within this readme.

The Eclipse Foundation supports granting permissions from triage to maintain permissions on Github, and Reporter to Maintainer on Gitlab. Owner permissions are not supported for either platform as they are not needed for typical project management scenarios.

_[^ Back to top](#eclipsefdn-github-sync)_  

## Github Sync  

Within Github, there is a mixed strategy for the management of projects within the space. Projects that are started while under the Eclipse umbrella or from a project that was incepted within the Eclipse ecosystem are by default created under the central Eclipse organization. On request, projects can be migrated to a separate organization that is still managed by the EclipseWebmaster account. Repositories or projects born from organizations or groups that have joined Eclipse Foundation post inception are usually managed under organizations managed by the EclipseWebmaster. While there are cases where projects can cross organizational bounds, it is uncommon (and covered by the sync script). 

Permissions to projects are managed through hidden teams that are then granted access to each repository for the given project within the current organization. For each organization that a project has repositories in, a set of the contributor, committer, and project-lead teams will be created to give access to those repositories. Each of these teams will have the same set of users as defined within the project management interface on projects.eclipse.org. For users to be properly added by this mechanism, they must set their Github Handle within their Eclipse Account on accounts.eclipse.org.

In regards to bot access, this is typically granted at the repository level, but can also be added at the team level if more broad access is needed. These permissions, while not removed by the script are currently managed manually by the Eclipse Foundation. If there are issues regarding bot access, new or existing, an issue should be created within our [bug-tracking system](https://bugs.eclipse.org) rather than within this project.

Below is an example of how a few projects may be managed within the Eclipse ecosystem:

```  
Eclipse/
├─ dash-licenses (project)
├─ technology-dash-contributors (private team, access to dash-licenses)
├─ technology-dash-committers (private team, access to dash-licenses)
├─ technology-dash-committers (private team, access to dash-licenses)
locationtech/
├─ spatial4j (project)
├─ locationtech-spatial4j-contributors (private team, access to spatial4j)
├─ locationtech-spatial4j-committers (private team, access to spatial4j)
├─ locationtech-spatial4j-committers (private team, access to spatial4j)
```

### Github Permissions mapping

Information on Github permissions is available in the [documentation for organizations](https://docs.github.com/en/github/setting-up-and-managing-organizations-and-teams/repository-permission-levels-for-an-organization).

| Eclipse Group | Github Permission Role|
|---|---|
| Contributor | Triage |
| Committer | Write  |
| Project Leads | Maintain |


_[^ Back to top](#eclipsefdn-github-sync)_

## Gitlab Sync
In Gitlab, a nested group strategy was chosen to manage access to both groups and projects. This gives greater control over inherited permissions without having to manage teams across multiple base groups. For each Open Source group with repositories managed by the Eclipse Foundation (such as Eclipse Foundation and the OpenHWGroup), a base group will exist to encapsulate all projects for that group. Within each of these groups, projects will have the option of choosing between 2 options when migrating over to their targeted projects' namespace:

1. Using the short project ID as the path of their group (i.e asciidoc-lang, dash). While the path of the project is determined programmatically, a display name may be chosen by the Project Leads of the project and a request made through the Eclipse Foundation Gitlab Helpdesk or requested at the time of the creation of the group.
2. Nesting the group mentioned in the first option within a group for the TLP (top-level project) of the given project. (i.e. asciidoc/asciidoc-lang, technology/dash). The name of the TLP group is determined by the Project Leads of the TLP, and similarly to the first option, a request may be made via the Eclipse Foundation Gitlab Helpdesk.

Within the namespace of your project, Project Leads may create further groups to better encapsulate their projects. This provides much better management of the subsequent projects through tools such as shared issue queues, kanban boards, milestones, and custom labels. Adding additional members outside of bot users to these subgroups is disallowed and will be cleaned regularly to ensure proper permissions are maintained.

In regards to bot access, this can be granted at either the subgroup or project (repository) level depending on the needs of the project. These permissions, while not removed by the script are currently managed manually by the Eclipse Foundation. If there are issues regarding bot access, new or existing, an issue should be created within our [bug-tracking system](https://bugs.eclipse.org) rather than within this project.

Below is an example of a few projects within the Eclipse Gitlab instance and their possible structure:

```  
Eclipse/ (group)
├─ Eclipse Dash/ (group)
│  ├─ dash-gitlab-testing (project)
│  ├─ org.eclipse.dash.handbook (project)
├─ Technology/ (group)
│  ├─ Eclipse Marketplace Client/ (group)
│  │  ├─ MPC Client (project)
│  │  ├─ org.eclipse.epp.mpc (project)
Eclipse Foundation/ (group)
├─ webdev/ (group)
│  ├─ eclipsefdn-api-common (project)

```  

One thing to note with the nested groups model. If project groups are nested under the TLP for said projects, the committers and Project Leads of the TLP will be functionally the same role for all projects nested beneath them. This is a side effect of inherited permissions, and while [not optimal, there is no IP exposure, and isn't a clear breaking of the EDP](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/42#note_828611).

### Gitlab Permissions mapping

More information on Gitlab permissions can be found in the [API documentation](https://docs.gitlab.com/ee/user/permissions.html).

| Eclipse Group | Gitlab Permission |
|---|---|
| Contributor | Reporter |
| Committer | Developer |
| Project Leads | Maintainer |
  
  
_[^ Back to top](#eclipsefdn-github-sync)_
## Usage
### Manual run parameters

The following parameters can be used when running the sync scripts manually.

#### Github params

| Parameter name | Required | Accepts | Default | Description |
|----------------|:--------:|---------|---------|-------------|
|-s, --secretLocation | ✓ | string | N/A | The location of the files containing API access tokens and secure configurations containing keys. |
|-c, --console | x | boolean flag | `false` | Adds console output to logging statements for the current run |
|-d, --dryRun | x | boolean flag | `false` | Runs script in dry run mode, not writing any changes to the API. |
|-D, --deletionDryRun | x | boolean flag | `false` | Runs script in deletion dry run mode, not performing any deletion operations to the API. |
|-t, --devMode | x | boolean flag | `false` | Runs the script with the dev mode active. This changes the returned data from the Eclipse API to affect a dev sandbox rather than production projects. |
|-p, --project | x |string | N/A | The project ID (e.g. technology.dash) of the project that should be updated (at the exclusion of all other projects) |
|-h, --help | x | N/A (flag) | N/A | Prints the help text for the script parameters. |
|-V, --verbose | x | boolean flag | `false` | Sets the script to run in verbose mode (ranges from 1-4 for more verbose logging). |

#### Gitlab params

| Parameter name | Required | Accepts | Default | Description |
|----------------|:--------:|---------|---------|-------------|
|-s, --secretLocation | ✓ | string | N/A | The location of the files containing API access tokens and secure configurations containing keys. |
|-c, --console | x | boolean flag | `false` | Adds console output to logging statements for the current run |
|-d, --dryRun | x | boolean flag | `false` | Runs script as dry run, not writing any changes to API. |
|-D, --devMode | x | boolean flag | `false` | Runs the script with the dev mode active. This changes the returned data from the Eclipse API to affect a dev sandbox rather than production projects. |
|-H, --host | x | string | `https://gitlab.eclipse.org` | The Gitlab host target for the sync. This allows for testing and staging migrations for use in development and dry runs. |
|-h, --help | x | N/A (flag) | N/A | Prints the help text for the script parameters. |
|-p, --provider | x | string | `oauth2_generic` | The OAuth provider name set in GitLab for the Eclipse Accounts binding. |
|-P, --project | x |string | N/A | The project ID (e.g. technology.dash) of the project that should be updated (at the exclusion of all other projects) |
|-r, --rootGroup | x |string | `eclipse` | The path of the root project to use when syncing PMI permissions. This is used to help in scoping retrievals and caching. |
|-V, --verbose | x | boolean flag | `false` | Sets the script to run in verbose mode (ranges from 1-4 for more verbose logging). |

### Running the toolset for development

#### Running for Github 

By default, the script is run in docker containers to emulate the production environment (Openshift). This sync tool can be run in standard and verbose mode. The difference between the modes is that in verbose all log messages are printed to the STDOUT of the container.

Before running, an `api-token` file should be created that contains the GitHub API key to be used for connectivity. This should be created in a `secret` folder at the root of the project (this has been excluded from Git commits so there is less danger of pushing live keys to a public repository).

```
docker build -f Dockerfile -t ef/gh-test .
docker run -i --rm -v <fullpath to current project folder>/secrets:/run/secrets --env DRYRUN=true ef/gh-test
```

_[^ Back to top](#eclipsefdn-github-sync)_  

#### Running for Gitlab

The default state for the script is to run against production Gitlab, which isn't feasible for development/testing. To better enable testing, a local instance of Gitlab should be started. This can be started using docker-compose: `docker-compose up -d gitlab`. This starts and binds a Gitlab instance to ports 22, 80, and 443 to best emulate a real instance of Gitlab. This can be changed in the docker-compose if needed.

Once a local instance is started, retrieve the password set for the root admin user using the following command: `sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password`. This will print the root user password as plain text to the console for use. Once retrieved, you can log in and update the password to something more memorable or easy to remember or set the password into a password manager. 

When you are logged in, you will need to create a base group to test the sync on. Using `eclipse` requires no additional config and is recommended. Once this group is created, any groups needed for projects should be added as they are currently not created if missing. For devMode, this group will be the group `spider.pig` under the `eclipse` group.


##### Gitlab Secrets

This step assumes the previous Gitlab setup to be completed, or you have access to the admin account running on the target instance. Open the [user preferences](http://localhost/-/profile/preferences) page and navigate to the `Access tokens` section. Once here, create an access token with the `api` permission set. This is needed to update or remove user permissions from groups and projects within the instance. Save this under your secrets directory within the file `access-token`.

Additionally, an `eclipse-oauth-config` should be created when running the GitLab sync script. This file will define how connections to the Eclipse OAuth server should be handled. If this is missing, the GitLab sync script will fail. The file should be in JSON format, with the following being an example of the format:

```
{"oauth":{"timeout":3600, "client_id":"<client id>","client_secret":"<client secret>","endpoint":"https://accounts.eclipse.org","scope":"eclipsefdn_view_all_profiles","redirect":"http://localhost"}}
```

Should you not have access to these values and you are on the EF Webdev team, reach out on slack and they will be provided. Otherwise, these credentials are internal and not distributed to the general public.

##### Gitlab script run command

Included below is an example command that can be used within Unix environments given the default setup instructions are followed:

```
npm run lab-sync -- --devMode --verbose --secretLocation=$PWD/secrets -H "http://localhost"
```


_[^ Back to top](#eclipsefdn-github-sync)_  
## Creating a tagged release

Once a release candidate has been identified for production, tests should be run locally using the master branch to ensure that the integration of patches are stable as the pull requests that had been validated. Additionally, `npm ci && npm run test` should be run to ensure that the current package lock file is stable, passes tests, and contains no live vulnerabilities from upstream packages. If there are issues with this, patches should be created to address these issues before the release goes live.

When determining the version bump [semantic versioning spec](https://semver.org/) should be used, which also forms the base of how [NPM versioning works](https://docs.npmjs.com/about-semantic-versioning). The new version should be applied through a PR on the master branch. Once this is done a new tag can be created on the master branch with the format `v#.#.#`, where the numbers should match the version stated in the package.json file. While there is no anticipated need to create an NPM package release, it is good practice and can help keep code clean. 

Once the master branch tag is created it can then be merged into the `production` branch. Pushing code to this branch triggers a build to update the running containers that manage this script. The golive process should be coordinated with the Releng team to ensure that support and additional eyes are available to help validate the changes. If there are issues regarding the new version, the script should be rolled back to the previous version to avoid service interruptions via pushes to the `production` branch.

## Maintainers

**Martin Lowe (Eclipse Foundation)**

- <https://github.com/autumnfound>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2019, 2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [eclipsefdn-github-sync authors](https://github.com/eclipsefdn/eclipsefdn-github-sync/graphs/contributors). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://github.com/eclipsefdn/eclipsefdn-github-sync/blob/master/LICENSE).


_[^ Back to top](#eclipsefdn-github-sync)_
