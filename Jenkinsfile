@Library('common-shared') _

pipeline {
  agent {
    kubernetes {
      label 'buildenv-agent'
      yaml '''
      apiVersion: v1
      kind: Pod
      spec:
        containers:
        - name: buildcontainer
          image: eclipsefdn/stack-build-agent:h111.3-n18.16-jdk11
          imagePullPolicy: Always
          command:
          - cat
          tty: true
          resources:
            requests:
              cpu: "1"
              memory: "4Gi"
            limits:
              cpu: "2"
              memory: "4Gi"
          env:
          - name: "HOME"
            value: "/home/jenkins"
          - name: "MAVEN_OPTS"
            value: "-Duser.home=/home/jenkins"
          volumeMounts:
          - name: m2-repo
            mountPath: /home/jenkins/.m2/repository
          - name: m2-secret-dir
            mountPath: /home/jenkins/.m2/settings.xml
            subPath: settings.xml
            readOnly: true
          - mountPath: "/home/jenkins/.m2/settings-security.xml"
            name: "m2-secret-dir"
            readOnly: true
            subPath: "settings-security.xml"
          - mountPath: "/home/jenkins/.mavenrc"
            name: "m2-dir"
            readOnly: true
            subPath: ".mavenrc"
          - mountPath: "/home/jenkins/.m2/wrapper"
            name: "m2-wrapper"
            readOnly: false
          - mountPath: "/home/jenkins/.cache"
            name: "yarn-cache"
            readOnly: false
          - mountPath: "/home/jenkins/.npm"
            name: "npm-cache"
            readOnly: false
        - name: jnlp
          resources:
            requests:
              memory: "1024Mi"
              cpu: "500m"
            limits:
              memory: "1024Mi"
              cpu: "1000m"
        volumes:
        - name: "m2-dir"
          configMap:
            name: "m2-dir"
        - name: m2-secret-dir
          secret:
            secretName: m2-secret-dir
        - name: m2-repo
          emptyDir: {}
        - name: m2-wrapper
          emptyDir: {}
        - name: yarn-cache
          emptyDir: {}
        - name: npm-cache
          emptyDir: {}
      '''
    }
  }

  environment {
    NAMESPACE = 'foundation-internal-webdev-apps'
    IMAGE_NAME = 'eclipsefdn/eclipsefdn-github-sync'
    CONTAINER_NAME = 'eclipsefdn-github-sync'
    GL_IMAGE_NAME = 'eclipsefdn/eclipsefdn-gitlab-sync'
    GL_CONTAINER_NAME = 'eclipsefdn-gitlab-sync'
    BUI_IMAGE_NAME = 'eclipsefdn/eclipsefdn-import-backup'
    BUI_CONTAINER_NAME = 'eclipsefdn-import-backup'
    TAG_NAME = sh(
      script: """
        GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
        printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
      """,
      returnStdout: true
    )
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timeout(time: 30, unit: 'MINUTES') 
  }

  triggers {
    // build once a week to keep up with parents images updates
    cron('H H * * H')
  }

  stages {
    stage('Test sync scripts') {
      steps {
        container('buildcontainer') {
          readTrusted 'package.json'
          
          sh '''
            npm ci --no-cache && npm run test
          '''
        }
      }
    }

    stage('Build docker image') {
      agent {
        label 'docker-build'
      }
      steps {
        sh '''
          docker build --pull -t ${IMAGE_NAME}:${TAG_NAME} -t ${IMAGE_NAME}:latest .
          docker build --pull -t ${GL_IMAGE_NAME}:${TAG_NAME} -t ${GL_IMAGE_NAME}:latest -f Dockerfile.gitlab .
          docker build --pull -t ${BUI_IMAGE_NAME}:${TAG_NAME} -t ${BUI_IMAGE_NAME}:latest -f Dockerfile.import .
        '''
      }
    }

    stage('Push docker image') {
      when {
        branch 'production'
      }
      agent {
        label 'docker-build'
      }
      steps {
        withDockerRegistry([credentialsId: '04264967-fea0-40c2-bf60-09af5aeba60f', url: 'https://index.docker.io/v1/']) {
          sh '''
            docker push ${IMAGE_NAME}:${TAG_NAME}
            docker push ${IMAGE_NAME}:latest
            docker push ${GL_IMAGE_NAME}:${TAG_NAME}
            docker push ${GL_IMAGE_NAME}:latest
            docker push ${BUI_IMAGE_NAME}:${TAG_NAME}
            docker push ${BUI_IMAGE_NAME}:latest
          '''
        }
      }
    }

    stage('Deploy to cluster') {
      agent {
        kubernetes {
          label 'kubedeploy-agent'
          yaml '''
            apiVersion: v1
            kind: Pod
            spec:
              containers:
              - name: kubectl
                image: eclipsefdn/kubectl:okd-c1
                command:
                - cat
                tty: true
                resources:
                  limits:
                    cpu: 1
                    memory: 1Gi
                volumeMounts:
                - mountPath: "/home/default/.kube"
                  name: "dot-kube"
                  readOnly: false
              - name: jnlp
                resources:
                  limits:
                    cpu: 1
                    memory: 1Gi
              volumes:
              - name: "dot-kube"
                emptyDir: {}
          '''
        }
      }
      when {
        branch 'production'
      }
      steps {
        container('kubectl') {
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=github-sync",
            kind: "cronjob.v1.batch",
            containerName: "${env.CONTAINER_NAME}",
            newImageRef: "${env.IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=gitlab-sync",
            kind: "cronjob.v1.batch",
            containerName: "${env.GL_CRONJOB_NAME}",
            newImageRef: "${env.GL_IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "job=github-org-backup",
            kind: "cronjob.v1.batch",
            containerName: "${env.BUI_CONTAINER_NAME}",
            newImageRef: "${env.BUI_IMAGE_NAME}:${env.TAG_NAME}"
          ])
        }
      }
    }
  }

  post {
    always {
      deleteDir() /* clean up workspace */
      sendNotifications currentBuild
    }
  }
}
