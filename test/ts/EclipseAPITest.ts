import { expect } from 'chai';
import { EclipseAPI } from '../../src/eclipse/EclipseAPI';

describe('EclipseAPI', function() {
  const eclipseAPI = new EclipseAPI({});
  describe('#eclipseAPI', function() {
    describe('success', function() {
      let result;
      before(async function() {
        // get eclipse projects, disable pagination as this is a long process
        result = await eclipseAPI.eclipseAPI('', false);
      });

      it('should contain JSON data', function() {
        expect(result).to.be.an('array');
      });
      it('should contain project_id field', function() {
        if (result.length > 0) {
          expect(result[0]).to.have.property('project_id').that.is.a('string');
        }
      });
      it('should contain github_repos field', function() {
        if (result.length > 0) {
          expect(result[0]).to.have.property('github_repos').that.is.an('array');
        }
      });
    });
  });

  describe('#eclipseBots', function() {
    describe('success', function() {
      let result;
      before(async function() {
        // get the eclipse bots for api
        result = await eclipseAPI.eclipseBots();
      });

      it('should contain JSON data', function() {
        expect(result).to.be.an('array');
      });
      it('should contain projectId string value', function() {
        // validate data required in further calls.
        expect(result[0]).to.have.property('projectId').that.is.a('string');
      });
    });
  });

  describe('#processBots', function() {
    describe('success', function() {
      let result;
      let bots;
      let siteName;
      before(async function() {
        // get current bots and find a site name to filter on
        bots = [
          {
            id: 1,
            projectId: 'ecd.che',
            username: 'genie.che',
            email: 'che-bot@eclipse.org',
            'github.com': {
              username: 'che-bot',
              email: 'che-bot@eclipse.org',
            },
            'github.com-openshift-ci-robot': {
              username: 'openshift-ci-robot',
              email: 'openshift-ci-robot@users.noreply.github.com',
            },
            'github.com-openshift-merge-robot': {
              username: 'openshift-merge-robot',
              email: 'openshift-merge-robot@users.noreply.github.com',
            },
            'non-gh-bot-sample': {
              username: 'non-gh-bot',
              email: 'non-gh-bot@test.com',
            },
          },
          {
            id: 11,
            projectId: 'eclipse.jdt',
            username: 'genie.jdt',
            email: 'jdt-bot@eclipse.org',
            'oss.sonatype.org': {
              username: 'jdt-dev',
              email: 'jdt-dev@eclipse.org',
            },
          },
        ];
        siteName = 'github.com';
        // get bots with filtered list
        result = eclipseAPI.processBots(bots, siteName);
      });

      it('should contain object data', function() {
        expect(result).to.be.an('object');
      });
      it('should contain keys that match bot projects in the original data', function() {
        // get the project IDs of bots that have an entry for the current site
        const projectNames = [];
        bots.forEach(b => (b[siteName] != undefined ? projectNames.push(b.projectId) : null));
        expect(result).to.have.keys(projectNames);
      });
      it('should contain keys that have at least 1 user associated', function() {
        for (const resultIdx in result) {
          expect(result[resultIdx]).to.be.an('array').that.is.not.empty;
        }
        // expect that the JDT project isn't included (no github.com bot)
        expect(result['eclipse.jdt']).to.be.undefined;
      });
      it('should contain keys that only have valid users associated', function() {
        // check that the ECD Che project gets included
        expect(result['ecd.che']).to.include.members(['openshift-ci-robot', 'openshift-merge-robot', 'che-bot']);
      });
    });
  });
});
